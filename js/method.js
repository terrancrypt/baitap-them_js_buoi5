// Hàm lấy dữ liệu từ id của giao diện
function domId(id) {
  return document.getElementById(id);
}

// Hàm in kết quả ra giao diện
function inRaGiaoDien(id, noiDung) {
  document.getElementById(id).innerHTML = noiDung;
}

// Bài 1: TÍnh tiền thuế
// Hàm tính tổng tiền thuế
function tinhThue(tongThuNhap, soNguoiPhuThuoc) {
  var thuNhapChiuThue = 0;
  var thuePhaiNop = 0;

  thuNhapChiuThue = tongThuNhap - 4e6 - soNguoiPhuThuoc * 16e5;
  if (thuNhapChiuThue <= 60e6) {
    thuePhaiNop = (thuNhapChiuThue * 5) / 100;
  } else if (thuNhapChiuThue > 60e6 && thuNhapChiuThue <= 120e6) {
    thuePhaiNop = (thuNhapChiuThue * 10) / 100;
  } else if (thuNhapChiuThue > 120e6 && thuNhapChiuThue <= 210e6) {
    thuePhaiNop = (thuNhapChiuThue * 15) / 100;
  } else if (thuNhapChiuThue > 210e6 && thuNhapChiuThue <= 384e6) {
    thuePhaiNop = (thuNhapChiuThue * 20) / 100;
  } else if (thuNhapChiuThue > 384e6 && thuNhapChiuThue <= 624e6) {
    thuePhaiNop = (thuNhapChiuThue * 25) / 100;
  } else if (thuNhapChiuThue > 624e6 && thuNhapChiuThue <= 960e6) {
    thuePhaiNop = (thuNhapChiuThue * 30) / 100;
  } else if (thuNhapChiuThue > 960e6) {
    thuePhaiNop = (thuNhapChiuThue * 35) / 100;
  }

  return thuePhaiNop;
}

// Bài 2:
// Hàm ẩn hiện số kết nối khi chọn doanh nghiệp
function anHien() {
  var x = domId("loaiKhachHang").value;
  if (x == 2) {
    domId("soKetNoi").classList.remove("d-none");
  } else {
    domId("soKetNoi").classList.add("d-none");
  }
}

// Hàm xử tính tiền cáp
function tinhTienCap(loaiKhachHang, soKenhCaoCap, soKetNoi) {
  var phiXuLi = 0;
  var phiDichVu = 0;
  var phiThueKenh = 0;
  var tienCap = 0;

  switch (loaiKhachHang) {
    case 1:
      phiXuLi = 4.5;
      phiDichVu = 20.5;
      phiThueKenh = 7.5;
      break;
    case 2:
      phiXuLi = 15;
      if (soKetNoi <= 10) {
        phiDichVu = 75;
      } else {
        phiDichVu = 75 + (soKetNoi - 10) * 5;
      }
      phiThueKenh = 50;
      break;
    default:
      alert("Bạn chưa chọn loại khách hàng");
  }

  return (tienCap = phiXuLi + phiDichVu + soKenhCaoCap * phiThueKenh);
}
