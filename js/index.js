// BÀI 1: Tính thuế thu nhập cá nhân
domId("btnTinhTienThue").onclick = function () {
  // Input:
  var tenNguoiDung = domId("tenNguoiDung").value;
  var tongThuNhap = Number(domId("tongThuNhap").value);
  var soNguoiPhuThuoc = Number(domId("soNguoiPhuThuoc").value);

  // Xử lý
  tienThue = tinhThue(tongThuNhap, soNguoiPhuThuoc);

  // In ra giao diện
  inRaGiaoDien(
    "tienThue",
    "Họ tên: " +
      tenNguoiDung +
      "; Tiền thuế thu nhập cá nhân: " +
      tienThue.toLocaleString()
  );
};

// BÀI 2:
domId("btnTinhTienCap").onclick = function () {
  var loaiKhachHang = Number(domId("loaiKhachHang").value);
  var soKetNoi = Number(domId("soKetNoi").value);
  var maKhachHang = domId("maKhachHang").value;
  var soKenhCaoCap = Number(domId("soKenhCaoCap").value);

  tienCap = tinhTienCap(loaiKhachHang, soKenhCaoCap, soKetNoi);

  inRaGiaoDien(
    "tienCap",
    "Mã khách hàng: " +
      maKhachHang +
      "; Tiền cáp: " +
      new Intl.NumberFormat("en-US", {
        style: "currency",
        currency: "USD",
        minimumFractionDigits: 2,
      }).format(tienCap)
  );
};
